<?php
$f = '/opt/bitnami/wordpress/wp-config.php';
$c = file_get_contents($f);

preg_match('/define.*DB_NAME.*\'(.*)\'/', $c, $m);
$dbname = $m[1];

preg_match('/define.*DB_USER.*\'(.*)\'/', $c, $m);
$dbuser = $m[1];

preg_match('/define.*DB_PASSWORD.*\'(.*)\'/', $c, $m);
$dbpass = $m[1];

preg_match('/define.*DB_HOST.*\'(.*)\'/', $c, $m);
$dbhost = $m[1];

$str=file_get_contents('/wp-config.php');
$str=str_replace("database_name_here", $dbname, $str);
$str=str_replace("username_here", $dbuser, $str);
$str=str_replace("password_here", $dbpass, $str);
$str=str_replace("localhost", $dbhost, $str);

file_put_contents('/wp-config.php', $str);

?>